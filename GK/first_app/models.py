from django.db import models

# Create your models here.
class Navbar(models.Model):
    Home = models.CharField(max_length=255)
    HomeA= models.BooleanField(default='True')
    Second = models.CharField(max_length=255)
    SecondA= models.BooleanField(default='True')
    Third = models.CharField(max_length=255)
    ThirdA= models.BooleanField(default='True')
    Forth = models.CharField(max_length=255)
    ForthA= models.BooleanField(default='True')
    Fifth = models.CharField(max_length=255)
    FifthA= models.BooleanField(default='True')
    Sixth = models.CharField(max_length=255)
    SixthA= models.BooleanField(default='True')
    def __str__(self):
        return self.Home
class Homepage(models.Model):
    First = models.CharField(max_length=255)
    FirstA= models.BooleanField(default='True')
    Second = models.CharField(max_length=255)
    SecondA= models.BooleanField(default='True')
    Banner = models.ImageField(upload_to='Home')
    def __str__(self):
        return self.First
class Secondpage(models.Model):
    Heading = models.CharField(max_length=255)
    subHeading=models.CharField(max_length=255)
    subHeadingA=models.BooleanField(default='True')
    div1Heading=models.CharField(max_length=255)
    div1SubHeading=models.CharField(max_length=255)
    div1Button=models.CharField(max_length=255)
    div1ButtonA=models.BooleanField(default='True')
    div2Heading=models.CharField(max_length=255)
    div2SubHeading=models.CharField(max_length=255)
    div2Button=models.CharField(max_length=255)
    div2ButtonA=models.BooleanField(default='True')
    div3Heading=models.CharField(max_length=255)
    div3SubHeading=models.CharField(max_length=255)
    div3Button=models.CharField(max_length=255)
    div3ButtonA=models.BooleanField(default='True')
    def __str__(self):
        return self.Heading
class Thirdpage(models.Model):
    Heading = models.CharField(max_length=255,blank = True)
    subHeading=models.CharField(max_length=255,blank = True)
    div1Pic = models.ImageField(upload_to='Thirdpage')
    div1Heading=models.CharField(max_length=255)
    div1SubHeading=models.CharField(max_length=255)
    div2Pic = models.ImageField(upload_to='Thirdpage',blank = True)
    div2Heading=models.CharField(max_length=255,blank = True)
    div2SubHeading=models.CharField(max_length=255,blank = True)
    div3Pic = models.ImageField(upload_to='Thirdpage',blank = True)
    div3Heading=models.CharField(max_length=255,blank = True)
    div3SubHeading=models.CharField(max_length=255,blank = True)
    div4Pic = models.ImageField(upload_to='Thirdpage',blank = True)
    div4Heading=models.CharField(max_length=255,blank = True)
    div4SubHeading=models.CharField(max_length=255,blank = True)
    def __str__(self):
        return self.Heading
class Forthpage(models.Model):
    Heading = models.CharField(max_length=255,blank = True)
    subHeading=models.CharField(max_length=255,blank = True)
    div1Pic = models.ImageField(upload_to='Forthpage')
    div1Heading=models.CharField(max_length=255)
    div1SubHeading=models.CharField(max_length=255)
    div1Button=models.CharField(max_length=255)
    div1ButtonA=models.BooleanField(default='True')
    div2Pic = models.ImageField(upload_to='Forthpage',blank = True)
    div2Heading=models.CharField(max_length=255,blank = True)
    div2SubHeading=models.CharField(max_length=255,blank = True)
    div2Button=models.CharField(max_length=255,blank = True)
    div2ButtonA=models.BooleanField(default='True',blank = True)
    div3Pic = models.ImageField(upload_to='Forthpage',blank = True)
    div3Heading=models.CharField(max_length=255,blank = True)
    div3SubHeading=models.CharField(max_length=255,blank = True)
    div3Button=models.CharField(max_length=255,blank = True)
    div3ButtonA=models.BooleanField(default='True',blank = True)
    def __str__(self):
        return self.Heading
class Fifthpage(models.Model):
    Heading = models.CharField(max_length=255)
    subHeading=models.CharField(max_length=255)
    FirstProgrssText = models.CharField(max_length=255)
    Firstpercentage =models.IntegerField(default="0")
    SecondProgrssText = models.CharField(max_length=255)
    Secondpercentage =models.IntegerField(default="0")
    ThirdProgrssText = models.CharField(max_length=255)
    Thirdpercentage =models.IntegerField(default="0")
    ForthProgrssText = models.CharField(max_length=255)
    Forthpercentage =models.IntegerField(default="0")
    def __str__(self):
        return self.Heading
class Contactpage(models.Model):
    Heading = models.CharField(max_length=255)
    subHeading=models.CharField(max_length=255)
    gotquestA=models.BooleanField(default='True')
    def __str__(self):
        return self.Heading
class Footerbar(models.Model):
    AboutUstext = models.CharField(max_length=255)
    Email=models.EmailField(max_length=254)
    StayConnectedtext = models.CharField(max_length=255)
    def __str__(self):
        return self.Email
