from django.shortcuts import render
from django.views.generic import TemplateView
from django.views.generic import (TemplateView,ListView,
                                  DetailView,CreateView,
                                  UpdateView,DeleteView)
from .models import Navbar,Homepage,Secondpage,Thirdpage,Forthpage,Fifthpage,Contactpage,Footerbar
# Create your views here.
def index(request):
    return render(request,'first_app/home.html')

class HomeView(ListView):
    template_name = 'first_app/home.html'
    context_object_name = 'Nav_list'
    queryset = Navbar.objects.all().filter(id=1)
    def get_context_data(self, **kwargs):
         context = super(HomeView, self).get_context_data(**kwargs)
         context['Home_list'] = Homepage.objects.all().filter(id=1)
         context['Second_list'] = Secondpage.objects.all().filter(id=1)
         context['Third_list'] = Thirdpage.objects.all()
         context['Forth_list'] = Forthpage.objects.all()
         context['Fifth_list'] = Fifthpage.objects.all().filter(id=1)
         context['Contact_list'] = Contactpage.objects.all().filter(id=1)
         context['Foot_list'] = Footerbar.objects.all().filter(id=1)
         return context
