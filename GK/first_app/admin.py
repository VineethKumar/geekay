from django.contrib import admin
from .models import Navbar,Homepage,Secondpage,Thirdpage,Forthpage,Fifthpage,Contactpage,Footerbar

# Register your models here.
admin.site.register(Navbar)
admin.site.register(Homepage)
admin.site.register(Secondpage)
admin.site.register(Thirdpage)
admin.site.register(Forthpage)
admin.site.register(Fifthpage)
admin.site.register(Contactpage)
admin.site.register(Footerbar)
